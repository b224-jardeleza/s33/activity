// #3
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json));


// #4
fetch('https://jsonplaceholder.typicode.com/todos').then(response => response.json())
.then(todo => { todo.map((item) => {
   	console.log(item.title); 
  });
});


// #5
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json));


// #6
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => 
	console.log(`The item '${json.title}' on the list has a status of ${json.completed}`)
);


// #7
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// #8
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		id: 1,
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// #9
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1,
		id: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// #10
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// #11
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "delectus aut autem",
		status: "Complete",
		dateCompleted: "07/09/21",
		userId: 1,
		id: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// #12
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})
.then((response) => response.json())
.then(json => console.log(json));